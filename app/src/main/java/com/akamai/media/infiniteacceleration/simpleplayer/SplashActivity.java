package com.akamai.media.infiniteacceleration.simpleplayer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Copyright © 2017 JUMP TV SOLUTIONS S.L. All rights reserved.
 */

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, DemoPlayerApp.class);
        startActivity(intent);
        finish();
    }
}
