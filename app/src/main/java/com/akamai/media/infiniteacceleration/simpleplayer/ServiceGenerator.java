package com.akamai.media.infiniteacceleration.simpleplayer;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ServiceGenerator {
    public static final String BASE_URL = "https://racenetapp.s3.amazonaws.com/html/en/app/racenet/";
    public static final String BASE_API_SEARCH_CATEGORY_URL = "http://volvo-racenet.qiupreview.com/en/app/racenet/search/category.json?category=";
    public static final String BASE_API_SEARCH_TEXT_URL = "http://volvo-racenet.qiupreview.com/en/app/racenet/search/text.json?txt=";
//    public static final String BASE_URL = "http://192.168.101.1:8081/race/";

             private static Retrofit.Builder builder =
             new Retrofit.Builder()
             .baseUrl(BASE_URL)
                     .addConverterFactory(GsonConverterFactory.create());
             private static OkHttpClient httpClient =
             new OkHttpClient.Builder().build();

             public static <S> S createService(Class<S> serviceClass) {
         builder.client(httpClient);
         Retrofit retrofit = builder.build();
         return retrofit.create(serviceClass);
         }
}
