
package com.akamai.media.infiniteacceleration.simpleplayer.model.live;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OctoshapeSetting {

    @SerializedName("OCTOLINK")
    @Expose
    private String oCTOLINK;
    @SerializedName("OCTOLINK_PHONE")
    @Expose
    private String oCTOLINKPHONE;
    @SerializedName("OCTOLINK_AUTH")
    @Expose
    private String oCTOLINKAUTH;
    @SerializedName("AUTH_WITH_AUTHID_URL")
    @Expose
    private String aUTHWITHAUTHIDURL;
    @SerializedName("AUTH_WITHOUT_AUTHID_URL")
    @Expose
    private String aUTHWITHOUTAUTHIDURL;
    @SerializedName("AUDIO_URL")
    @Expose
    private String aUDIOURL;

    public String getOCTOLINK() {
        return oCTOLINK;
    }

    public void setOCTOLINK(String oCTOLINK) {
        this.oCTOLINK = oCTOLINK;
    }

    public String getOCTOLINKPHONE() {
        return oCTOLINKPHONE;
    }

    public void setOCTOLINKPHONE(String oCTOLINKPHONE) {
        this.oCTOLINKPHONE = oCTOLINKPHONE;
    }

    public String getOCTOLINKAUTH() {
        return oCTOLINKAUTH;
    }

    public void setOCTOLINKAUTH(String oCTOLINKAUTH) {
        this.oCTOLINKAUTH = oCTOLINKAUTH;
    }

    public String getAUTHWITHAUTHIDURL() {
        return aUTHWITHAUTHIDURL;
    }

    public void setAUTHWITHAUTHIDURL(String aUTHWITHAUTHIDURL) {
        this.aUTHWITHAUTHIDURL = aUTHWITHAUTHIDURL;
    }

    public String getAUTHWITHOUTAUTHIDURL() {
        return aUTHWITHOUTAUTHIDURL;
    }

    public void setAUTHWITHOUTAUTHIDURL(String aUTHWITHOUTAUTHIDURL) {
        this.aUTHWITHOUTAUTHIDURL = aUTHWITHOUTAUTHIDURL;
    }

    public String getAUDIOURL() {
        return aUDIOURL;
    }

    public void setAUDIOURL(String aUDIOURL) {
        this.aUDIOURL = aUDIOURL;
    }

}
