
package com.akamai.media.infiniteacceleration.simpleplayer.model.live;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Example {

    @SerializedName("octoshape-settings")
    @Expose
    private List<OctoshapeSetting> octoshapeSettings = null;

    public List<OctoshapeSetting> getOctoshapeSettings() {
        return octoshapeSettings;
    }

    public void setOctoshapeSettings(List<OctoshapeSetting> octoshapeSettings) {
        this.octoshapeSettings = octoshapeSettings;
    }

}
