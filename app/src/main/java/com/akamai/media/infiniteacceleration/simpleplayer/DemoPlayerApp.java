package com.akamai.media.infiniteacceleration.simpleplayer;


import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.akamai.media.infiniteacceleration.simpleplayer.players.InfiniteVideoView;
import com.akamai.media.infiniteacceleration.simpleplayer.players.MediaControllerView;
import com.akamai.media.infiniteacceleration.simpleplayer.model.live.Example;
import com.google.gson.Gson;
import com.octoshape.android.client.OctoStatic;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import octoshape.osa2.Problem;
import octoshape.osa2.android.OctoshapeSystem;
import octoshape.osa2.android.StreamPlayer;
import octoshape.osa2.listeners.OctoshapeSystemListener;
import octoshape.osa2.listeners.ProblemListener;
import okhttp3.ResponseBody;
import retrofit2.Call;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DemoPlayerApp extends AppCompatActivity implements OctoshapeSystemListener {

    @Bind(R.id.materialProgressBar)
    MaterialProgressBar materialProgressBar;
    @Bind(R.id.controlLayout)
    MediaControllerView controlLayout;
    public String uri;
    public String json_name;
    @Bind(R.id.imageButton)
    ImageView imageButton;
    @Bind(R.id.guiLayout)
    RelativeLayout guiLayout;
    private OctoshapeSystem os;
    private Stream currentStream;
    protected MediaControllerView mControlView;
    protected InfiniteVideoView mVideoView;
    protected byte currentStatus;
    protected Handler myHandler;
    List<Stream> mPlayList = new ArrayList<Stream>();
    boolean show_image = true;
    private MaterialDialog error_dialog;
    private int current_volume;
    private AudioManager am;

    @Bind(R.id.toolbar2)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.main);
        ButterKnife.bind(this);
        mControlView = (MediaControllerView) findViewById(R.id.controlLayout);
        mControlView.setmPauseButton((ImageView) findViewById(R.id.imageButton));
        mVideoView = (InfiniteVideoView) findViewById(R.id.videoView);
        mVideoView.setProblemListener(mProblemListener);
        json_name = getIntent().getStringExtra("json");
        uri = getIntent().getStringExtra("uri");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        myHandler = new Handler();
        guiLayout.setOnClickListener((view) -> {
            if (toolbar.getVisibility() != View.VISIBLE) {
                toolbar.setAnimation(AnimationUtils.loadAnimation(DemoPlayerApp.this, R.anim.slide_top_in));
                toolbar.setVisibility(View.VISIBLE);
            } else {
                toolbar.setAnimation(AnimationUtils.loadAnimation(DemoPlayerApp.this, R.anim.slide_top_out));
                toolbar.setVisibility(View.GONE);
            }
            if (imageButton.getVisibility() != View.VISIBLE) {
                imageButton.setAnimation(AnimationUtils.loadAnimation(DemoPlayerApp.this, R.anim.fade_in));
                imageButton.setVisibility(View.VISIBLE);
            } else {
                imageButton.setAnimation(AnimationUtils.loadAnimation(DemoPlayerApp.this, R.anim.fade_out));
                imageButton.setVisibility(View.GONE);
            }
            new Handler().postDelayed(() -> {
                if (toolbar.getVisibility() == View.VISIBLE) {
                    toolbar.setAnimation(AnimationUtils.loadAnimation(DemoPlayerApp.this, R.anim.slide_top_out));
                    toolbar.setVisibility(View.GONE);
                }
            }, 3000);
            new Handler().postDelayed(() -> {
                if (imageButton.getVisibility() == View.VISIBLE) {
                    imageButton.setAnimation(AnimationUtils.loadAnimation(DemoPlayerApp.this, R.anim.fade_out));
                    imageButton.setVisibility(View.GONE);
                }
            }, 3000);
            if (mVideoView != null && mVideoView.isPlaying()) {
                if (mControlView.getVisibility() != View.VISIBLE) {
                    mControlView.setAnimation(AnimationUtils.loadAnimation(DemoPlayerApp.this, R.anim.slide_botom_in));
                    mControlView.setVisibility(View.VISIBLE);
                } else {
                    mControlView.setAnimation(AnimationUtils.loadAnimation(DemoPlayerApp.this, R.anim.slide_bottom_out));
                    mControlView.setVisibility(View.GONE);
                }
                new Handler().postDelayed(() -> {
                    if (mControlView.getVisibility() == View.VISIBLE) {
                        mControlView.setAnimation(AnimationUtils.loadAnimation(DemoPlayerApp.this, R.anim.slide_bottom_out));
                        mControlView.setVisibility(View.GONE);
                    }
                }, 3000);
            }
        });
        getData();
    }

    private void requestAudio() {
        am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        // Request audio focus for playback
//        int result = am.requestAudioFocus(i -> {
//
//                },
//                // Use the music stream.
//                AudioManager.STREAM_MUSIC,
//                // Request permanent focus.
//                AudioManager.AUDIOFOCUS_GAIN);
//
//        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
//            // Start playback
//            current_volume= am.getStreamVolume(AudioManager.STREAM_MUSIC);
//            am.setStreamVolume(AudioManager.STREAM_MUSIC,am.getStreamMaxVolume(AudioManager.STREAM_MUSIC)/2,0);
        init();
//        }
    }

    private void init() {
        // Initialize InfiniteMediaAcceleration client
        os = OctoStatic.create(this, mProblemListener, null);
        os.setOctoshapeSystemListener(this);
        // Add available players
        os.addPlayerNameAndVersion(OctoshapeSystem.MEDIA_PLAYER_NATIVE,
                OctoshapeSystem.MEDIA_PLAYER_NATIVE, "" + Build.VERSION.SDK_INT);
        os.open();
    }

    Runnable probeStreamPlayer = new Runnable() {

        @Override
        public void run() {
            if (currentStream != null && currentStream.getStreamPlayer() != null) {
                if (currentStream.getStreamPlayer().getStatus() != currentStatus) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String status;
                            switch (currentStream.getStreamPlayer().getStatus()) {
                                case StreamPlayer.STATUS_CLOSED:
                                    status = "STATUS_CLOSED";
                                    showImage();
                                    break;
                                case StreamPlayer.STATUS_INITIALIZING:
                                    status = "STATUS_INITIALIZING";
                                    break;

                                case StreamPlayer.STATUS_PLAY_INITIALIZING:
                                    status = "STATUS_PLAY_INITIALIZING";
                                    break;

                                case StreamPlayer.STATUS_PLAYING:
                                    status = "STATUS_PLAYING";
                                    if (show_image)
                                        hideImage();
                                    break;

                                case StreamPlayer.STATUS_READY:
                                    status = "STATUS_READY";
                                    break;

                                case StreamPlayer.STATUS_UNINITIALIZED:
                                    status = "STATUS_UNINITIALIZED";
                                    changeToRefresh();
                                    break;

                                default:
                                    status = "UNKNOWN";
                                    changeToRefresh();
                                    break;
                            }
                        }
                    });
                    currentStatus = currentStream.getStreamPlayer().getStatus();
                }
            }
            myHandler.postDelayed(probeStreamPlayer, 500);
        }
    };

    private void hideImage() {
        show_image = false;
        materialProgressBar.setVisibility(View.INVISIBLE);
    }

    private void showImage() {
    }

    private void changeToRefresh() {
        materialProgressBar.setVisibility(View.INVISIBLE);
        error_dialog = new MaterialDialog.Builder(this).title(R.string.octo_error)
                .content(R.string.octo_error_content)
                .positiveText(R.string.retry)
                .positiveColor(getResources().getColor(R.color.colorPrimary))
                .onPositive((dialog, which) -> {
//                    Utils.toggleHideyBar(DemoPlayerApp.this);
                    playStream();
                })
                .build();
        if (error_dialog != null && !error_dialog.isShowing())
            error_dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnect(String authID) {
        // Connected to Octoshape Client, so initialize GUI
        runOnUiThread(new Runnable() {
            public void run() {
                playStream();
            }
        });
    }

    private ProblemListener mProblemListener = new ProblemListener() {
        public void gotProblem(final Problem p) {
            //Log.d(InfiniteAcceleration.LOGTAG, "Problem: " + p.toString());
            if (p.getMessage() != null)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast toast = Toast.makeText(DemoPlayerApp.this, "Problem: " + p.getMessage(),
                                Toast.LENGTH_LONG);
                        toast.show();
                    }
                });
        }
    };

    // A Stream was selected
    public void playStream() {
        materialProgressBar.setVisibility(View.VISIBLE);
        myHandler.removeCallbacks(probeStreamPlayer);
        myHandler.post(probeStreamPlayer);
        if (currentStream != null)
            currentStream.close();

        currentStream = mPlayList.get(0);
        //Log.d(InfiniteAcceleration.LOGTAG, "Requesting stream: " + currentStream.getName());
        mVideoView.reset();
        mVideoView.setVisibility(View.VISIBLE);
        mVideoView.setController(mControlView);
        mVideoView.playStream(os, currentStream, mProblemListener);
    }

    @Override
    public void onBackPressed() {
        shutdown();
//        if(am!=null)
//        am.setStreamVolume(AudioManager.STREAM_MUSIC,current_volume,0);
    }

    @Override
    public void onPause() {
        super.onPause();
        shutdown();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_BACK:
                return super.dispatchKeyEvent(event);
            default:
                return true;
        }
    }

    private void shutdown() {
        if (mVideoView != null)
            mVideoView.reset();
        if (currentStream != null)
            currentStream.close();
        myHandler.removeCallbacks(probeStreamPlayer);
        OctoStatic.terminate(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });
    }

    public void getData() {
        new Handler().postDelayed(() -> {
            Observable.create((Observable.OnSubscribe<String>) subscriber -> {
                try {
                    Call<ResponseBody> result = ServiceGenerator.createService(VolvoApi.class).getJsonFromUrl("http://www.volvooceanrace.com/en/app/octoshape-settings.json");
                    String data = result.execute().body().string();
                    subscriber.onNext(data);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                    subscriber.onCompleted();
                }
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<String>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            Log.d("ROOR FRAGMENT ERROR", "octho");
                            showErrorLoadDialog();
                        }

                        @Override
                        public void onNext(String gson) {
                            proccessData(gson);
                        }
                    });
        }, 200);
    }

    private void showErrorLoadDialog() {
        new MaterialDialog.Builder(this)
                .title(R.string.error)
                .content(R.string.load_content_error)
                .positiveText(R.string.tryy)
                .negativeText(R.string.cancel)
                .positiveColor(getResources().getColor(R.color.colorPrimary))
                .negativeColor(getResources().getColor(R.color.colorPrimary))
                .onPositive(((dialog, which) -> {
                    getData();
                }))
                .onNegative((dialog, which) -> dialog.dismiss()).build().show();
    }

    private void proccessData(String gson) {
        String stream_url = new Gson().fromJson(gson, Example.class).getOctoshapeSettings().get(0).getOCTOLINK();
        mPlayList.add(new Stream(stream_url, "ABR", "NBA - LIVE"));
//                mPlayList.add(new Stream("octoshape://streams.octoshape.net/demo/live/pga/abr", "ABR", "NBA - LIVE"));
        requestAudio();
    }

}
