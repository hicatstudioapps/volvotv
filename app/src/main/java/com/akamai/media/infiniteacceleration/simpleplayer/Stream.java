package com.akamai.media.infiniteacceleration.simpleplayer;

import android.net.Uri;
import android.util.Log;

import java.util.LinkedList;
import java.util.Queue;

import octoshape.osa2.TimeInfo;
import octoshape.osa2.TimeInfoLive;
import octoshape.osa2.android.OctoshapeSystem;
import octoshape.osa2.android.StreamPlayer;
import octoshape.osa2.android.listeners.MediaPlayerListener;
import octoshape.osa2.android.listeners.StreamPlayerListener;
import octoshape.osa2.listeners.ProblemListener;
import octoshape.osa2.listeners.TimeInfoListener;

public class Stream {

	public static final int MAX_PLAYER_BUF_MS = 30000;
    private String octolink;
    private String bitrate;
    private String name;
    private StreamPlayer sp;
    private long osaSeekOffset;
    private long osaDuration;
    private boolean isLive;
    private boolean nativeSeek;
    private String selectedPlayerId;
    private boolean osaSeek;
    private int offsetDVR;
    private long pausedPosition;

    public Queue<Uri> urlQueue = new LinkedList<Uri>();
    protected long maxDVR;
    public MediaPlayerListener mediaPlayerListener = null;
    public Stream(String octolink, String bitrate, String name) {
        super();
        this.octolink = octolink;
        this.bitrate = bitrate;
        this.name = name;
    }

    public String getOctolink() {
        return octolink;
    }

    public String getBitrate() {
        return bitrate;
    }

    public String getName() {
        return name;
    }

    public void playback(OctoshapeSystem os, ProblemListener mProblemListener, final StreamListener callback) {

        sp = os.createStreamPlayer(octolink);
        sp.setProblemListener(mProblemListener);
        sp.setListener(new StreamPlayerListener() {

            @Override
            public void gotUrl(final String url, final long seekOffset, MediaPlayerListener mpListener) {

                Log.d(InfiniteAcceleration.LOGTAG, "gotUrl: " + url);
                Log.d(InfiniteAcceleration.LOGTAG, "seekOffset: " + seekOffset);
                mediaPlayerListener = mpListener;
                osaSeekOffset = seekOffset;
                pausedPosition = 0;
                callback.onPrepared(Uri.parse(url), mpListener);
            }

            @Override
            public void gotNewOnDemandStreamDuration(long duration) {
                osaDuration = duration;
            }

            @Override
            public void resolvedNativeSeek(boolean live, String playerId) {
                Log.d(InfiniteAcceleration.LOGTAG, name
                        + ": resolvedNativeSeek: ");
                isLive = live;
                nativeSeek = true;
                selectedPlayerId = playerId;
                callback.onInit(osaSeek, nativeSeek,live,-1);
            }

            @Override
            public void resolvedNoSeek(boolean live, String playerId) {
                Log.d(InfiniteAcceleration.LOGTAG, name + ": resolvedNoSeek: ");
                isLive = live;
                selectedPlayerId = playerId;
                callback.onInit(osaSeek, nativeSeek,live, -1);
            }

            @Override
            public void resolvedOsaSeek(boolean live, long duration,
                                        String playerId) {
                Log.d(InfiniteAcceleration.LOGTAG, name + ": resolvedOsaSeek: ");
                isLive = live;
                nativeSeek = false;
                osaSeek = true;
                if (!isLive)
                    osaDuration = duration;
                else {
                    maxDVR = duration;
                }
                selectedPlayerId = playerId;
                callback.onInit(osaSeek, nativeSeek,live, duration);
            }
        });
        sp.requestPlay();
    }
    public long getOsaSeekOffset() {
        return osaSeekOffset;
    }

    public long getOsaDuration() {
        return osaDuration;
    }

    public boolean isLive() {
        return isLive;
    }

    public boolean isNativeSeek() {
        return nativeSeek;
    }

    public String getSelectedPlayerId() {
        return selectedPlayerId;
    }

    public boolean isOsaSeek() {
        return osaSeek;
    }
    public StreamPlayer getStreamPlayer() {
        return sp;
    }
    public long getMaxDVR(){
        return maxDVR;
    }
    public int getCurrentDVR() {
        return this.offsetDVR;
    }
    public void setCurrentDVR(int progress) {
        this.offsetDVR = progress;
    }
    public void close(){
        if( sp !=null )
            sp.close(null);
    }
    /**
     * The following examplifies pausing/resuming live streams by use of DVR, knowing 
     * the amount of unplayed mediadata residing in the MediaPlayer buffer
     * <p>
     * 	This is achieved by acquiring the exact time of the latest keyframe sent to the player.
     * 	Upon resume, mediadata from that time instance minus the duration of the unplayed playerBuffer
     *  is requested.
     * </p>
     * @param playerBuffer amount of unplayed mediadata residing in the MediaPlayer buffer in milliseconds.
     * 
     */
    public void pause(final long playerBuffer) {
        if(sp == null || !isLive)
            return;
        sp.requestTimeInfo(new TimeInfoListener() {
            @Override
            public void gotTimeInfo(TimeInfo timeInfo) {
                if (timeInfo.isLive) {
                    TimeInfoLive tl = (TimeInfoLive) timeInfo;
                    sp.requestPlayAbort();
                    pausedPosition = tl.lastFrame-playerBuffer;
                    Log.d("Stream", "Pausing at:" + pausedPosition + "(PlayerBuffer:"+playerBuffer+")");
                }
            }
        });
    }
    /**
     * The following examplifies pausing/resuming live streams by use of DVR.
     * @see {@link Stream#pause(long)}
     */
    public void pause() {
    	pause(5000);
    }
    
    public boolean isPaused(){
        return pausedPosition != 0;
    }
    /**
     * Resumes playback of a Live stream from the paused time instance.
     * <p>
     *  This will cause a new URL callback gotUrl() callback, which provides
     *  the exact time on which it was capable to resume playback.
     * </p>
     */
    public void resume(){
        if(sp != null & isPaused() && isLive()) {
            sp.requestPlayLiveTimePosition(pausedPosition);
            Log.d("Stream", "Resuming at:" + pausedPosition);
            pausedPosition = 0;
        }
    }
}