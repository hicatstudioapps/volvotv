package com.akamai.media.infiniteacceleration.simpleplayer;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by CQ on 24/10/2016.
 */

public interface VolvoApi {

    @GET
    public retrofit2.Call<ResponseBody> getJsonFromUrl(@Url String url);
}
