package com.akamai.media.infiniteacceleration.simpleplayer;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;


public class InfiniteAcceleration extends Application{
    public static String LOGTAG = "InfiniteAccelerationDemo";
    static List<Stream> mPlayList = new ArrayList<Stream>();

    @Override
    public void onCreate(){
        super.onCreate();
        mPlayList.add(new Stream("octoshape://streams.octoshape.net/demo/live/nba/abr", "ABR", "NBA - LIVE"));
        mPlayList.add(new Stream("octoshape://streams.octoshape.net/demo/live/nascar/abr", "ABR", "Nascar - LIVE"));
        mPlayList.add(new Stream("octoshape://streams.octoshape.net/demo/live/pga/abr", "ABR", "PGA - LIVE"));
        mPlayList.add(new Stream("octoshape://streams.octoshape.net/demoplayer/mobile/bbb/bbb_auto_mixedres", "ABR", "Big Buck Bunny - VOD"));
        mPlayList.add(new Stream("octoshape://streams.octoshape.net/demoplayer/mobile/sintel/auto_flv", "ABR", "Sintel - VOD"));
    }

    static public List<Stream> getPlayList(){
        return mPlayList;
    }
}

